class ViewReviewPage{
    clickViewReviewLink(){
        return cy.get('[width="100%"] > :nth-child(1) > :nth-child(2) > .instructions > table > tbody > :nth-child(1) > td > a > ')
    }
    enterEmail()
{
   return cy.get('#txtEmail')
}
enterTitle(){
   return cy.get('#selTitle')
}
enterFirstName(){
   return cy.get('#txtFirstName')
}
enterLastName(){
   return cy.get('#txtLastName')

}
selectCountry(){
    return cy.get('#selCountry')

}
enterInsitute(){
    return cy.get('#txtInstitution')
}


}
export default ViewReviewPage;