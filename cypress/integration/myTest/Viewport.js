describe('testing differrnt view ports',()=>{
    before(()=>{
        console.log('running my test')
    })
    beforeEach(()=>{
        cy.visit('https://www.google.co.in/')
    })
    it('open in macbook -13',()=>{
        cy.viewport('macbook-13')
        cy.screenshot()
        cy.wait(200)
    })
    it('open in macbook -11',()=>{
        cy.viewport('macbook-11')
        cy.screenshot()
        cy.wait(200)
    })
    it('open in iphone-x',()=>{
        cy.viewport('iphone-x')
        cy.screenshot()
        cy.wait(200)
    })
    it('open in iphone-x',()=>{
        cy.viewport(550,750)
        cy.screenshot()
        cy.wait(200)
    })
})