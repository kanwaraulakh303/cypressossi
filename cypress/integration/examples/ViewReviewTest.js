
import HomePage from '../../support/PageObjects/HomePage'
import ViewReviewPage from '../../support/PageObjects/ViewReviewPage'
describe('MySecondSuite', function() 
{   
    before(function() {
        // root-level hook
        // runs before every test
        cy.fixture('example').then(function(data){
       this.data=data;     

        })
     })
  
   it('verify Title of the Page', function()  {
    const homePage= new HomePage();
    const viewreviewPage=new ViewReviewPage()
    cy.visit(Cypress.env('url'));

   homePage.getUserName().type(this.data.username)
    homePage.getPassword().type(this.data.password)
    cy.get(':nth-child(4) > :nth-child(2) > .button').click();
   
   // Cypress.config('defaultCommandTimeout',1000)
   // cy.title().should('eq','Inderscience Submissions - Inderscience Publishers ')
   viewreviewPage.clickViewReviewLink().click();
   viewreviewPage.enterEmail().type(this.data.email)
  // viewreviewPage.enterTitle().select(this.data.title)
   viewreviewPage.enterTitle().select(this.data.title)
  viewreviewPage.enterFirstName().type(this.data.firstName)
  viewreviewPage.enterLastName().type(this.data.lastName)
   viewreviewPage.selectCountry().select(this.data.CountryName)
  viewreviewPage.enterInsitute().type(this.data.Institution)
   cy.get('#txtBiography').type("abhishek Albania oxford")
   cy.get('[value="380"]').check();
   cy.get('#cmdSubmit').click()




    //cy.get(':nth-child(7) > :nth-child(1) > table > tbody > tr > [width="50"] > #chInterest').should('be.visible')
  
 

 }) })