
/// <reference types="Cypress" />
import HomePage from '../../support/PageObjects/HomePage'
describe('verifyHomePageTitle', function() 
{   
  before(function() {
      // root-level hook
      // runs before every test
      cy.fixture('example').then(function(data){
     this.data=data;     

      })  })
      it('verify Title of the Page', function()  {
        const homePage= new HomePage();
         cy.visit(Cypress.env('url'));
         cy.url().should('eq', 'https://www.indersciencesubmissions.com/');
         homePage.getUserName().type(this.data.username)
         homePage.getPassword().type(this.data.password)
         cy.get(':nth-child(4) > :nth-child(2) > .button').click();
         cy.title().should('eq','Inderscience Submissions - Inderscience Publishers')

   })
 
  })